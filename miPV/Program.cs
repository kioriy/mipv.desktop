﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miPV
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new PuntoDeVenta());
            //Application.Run(new FormActividad());
            Application.Run(new frmInicioSession());
            //Application.Run(new AdministradorUsuarios());

        }
    }
}
