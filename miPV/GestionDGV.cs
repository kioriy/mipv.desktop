﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miPV
{
    class GestionDGV
    {
        #region variables;
        private DataGridView dgv;
        #endregion

        #region constructor
        public GestionDGV(DataGridView dgv)
        {
            this.dgv = dgv;
        }
        #endregion

        #region metodos

        public void inicializarDgv()
        {
            foreach (DataGridViewColumn col in this.dgv.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 16F, FontStyle.Bold, GraphicsUnit.Pixel);
            }

            //DataGridViewRow row = this.dgv.Rows[0];
            //row.Height = 35;

            this.dgv.RowsDefaultCellStyle.BackColor = Color.LightBlue;
            this.dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.White;

            dgv.Columns[1].Width = 90;
            dgv.Columns[2].Width = 220;
            dgv.Columns[3].Width = 90;
            dgv.Columns[4].Width = 90;
        }

        public void cargarColumnas()
        {
            dgv.ColumnCount = 5;

            dgv.Columns[0].HeaderText = "CODIGO";
            dgv.Columns[1].HeaderText = "CANTIDAD";
            dgv.Columns[2].HeaderText = "DESCRIPCION";
            dgv.Columns[3].HeaderText = "PRECIO U.";
            dgv.Columns[4].HeaderText = "TOTAL";

            dgv.Columns[0].Name = "cCodigo";
            dgv.Columns[1].Name = "cCantidad";
            dgv.Columns[2].Name = "cDescripcion";
            dgv.Columns[3].Name = "cPrecioUnitario";
            dgv.Columns[4].Name = "cTotal";

            dgv.Columns["cCodigo"].Visible = false;
        }

        public void cargarDgvProductos(Producto producto)
        {
            string idProducto = producto.IdProducto;
            string codigo = producto.Codigo;
            string precioCosto = producto.PrecioCosto;
            decimal precioVenta = Convert.ToDecimal(producto.PrecioVenta);
            string descripcion = producto.Descripcion;
            decimal cantidad = Convert.ToDecimal(producto.Cantidad);
            decimal total = precioVenta;

            dgv.Rows.Add(codigo, "1", descripcion, string.Format("{0:N2}", precioVenta),string.Format("{0:N2}", total));
        }

        public string sumarTotalPagado(DataGridView dgv)
        {
            double suma = 0;

            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells["cTotal"].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells["cTotal"].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            return string.Format("{0:N2}", suma);//suma.ToString("C"); 
        }

        public void sumarFilas(DataGridViewCellEventArgs e)
        {
            if (this.dgv.Columns[e.ColumnIndex].Name == "cCantidad")
            {
                decimal cantidad = Convert.ToDecimal(this.dgv.CurrentRow.Cells["cCantidad"].Value);
                decimal precioU = Convert.ToDecimal(this.dgv.CurrentRow.Cells["cPrecioUnitario"].Value);
                decimal total = cantidad * precioU;

                this.dgv.CurrentRow.Cells["cTotal"].Value = string.Format("{0:N2}", total);
            }
        }

        #endregion
    }
}
