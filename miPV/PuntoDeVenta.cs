﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miPV
{
    public partial class PuntoDeVenta : Form
    {
        GestionDGV dgv;
        DataTable tableIdProductos;
        Producto producto;
        Venta venta;
        DetalleVenta detalleVenta;
        TicketPrint ticketPrint;
        Caja caja;
        FormActividad formAct = new FormActividad();
        string usuario = "";
        string rol = "";

        #region constructor
        public PuntoDeVenta(string usuario, string rol)
        {
            InitializeComponent();

            inicializarVariables();
            dgv.cargarColumnas();
            dgv.inicializarDgv();
            crearColumna();

            this.usuario = usuario;
            this.rol = rol;
        }
        #endregion

        #region botones

        #region producto -> abre el formulario producto
        private void cmdProductos_Click(object sender, EventArgs e)
        {
            ProductoFrm productoFrm = new ProductoFrm();
            productoFrm.Show();
        }
        #endregion

        #region bonton Pagar -> guardar venta y detalle de venta
        private void cmdPagar_Click(object sender, EventArgs e)
        {
            decimal total = Convert.ToDecimal(lTotal.Text);
            decimal pago = Convert.ToDecimal(txtPago.Text);

            if (lTotal.Text != "0.0")
            {
                if(validar_cambio(total, pago))
                {
                    if (venta.insertarVenta())
                    {
                        if (!detalleVenta.insertarDetalleVenta())
                        {
                            venta.eliminarVenta();
                            detalleVenta.eliminarDetalleVenta();
                        }
                        else
                        {
                            ticketPrint = new TicketPrint();
                            ticketPrint.imprimirTicket(tableIdProductos, lTotal.Text);
                            limpiarRegistro();
                            crearNotificacion("Venta registrada EXITOSAMENTE!!", Color.Green);
                        }
                    }
                    else
                    {
                        crearNotificacion("La venta no se registro intente nuevamente", Color.Red);
                    }
                }
            }
            else
            {
                crearNotificacion("La tabla esta vacia, ingrese articulos para registrar la venta", Color.Red);
            }
        }
        #endregion

        #region buscar -> crea el efecto de presionado del boton buscar
        private void cmdBuscarProducto_Click(object sender, EventArgs e)
        {
            if (cmdBuscarProducto.BackColor == Color.Transparent)
            {
                cmdBuscarProducto.BackColor = Color.Lime;
                crearNotificacion("Boton de busqueda ACTIVADO", Color.Green);
            }
            else
            {
                cmdBuscarProducto.BackColor = Color.Transparent;
                limpiarNotificaciones();
            }
        }
        #endregion

        #region corte de caja
        private void cmdCorteCaja_Click(object sender, EventArgs e)
        {
            if (cmdCorteCaja.BackColor == Color.Transparent)
            {
                lMontoCorte.Text = "$ ";
                cmdCorteCaja.BackColor = Color.Lime;
                crearNotificacion("Boton de corte de caja ACTIVADO", Color.Green);

                dtpFecha.Visible = true;
                lMontoCorte.Visible = true;

                string fecha = dtpFecha.Value.ToString("dd/MM/yyyy");

                lMontoCorte.Text += caja.corte(fecha);
            }
            else
            {
                cmdCorteCaja.BackColor = Color.Transparent;
                limpiarNotificaciones();

                dtpFecha.Visible = false;
                lMontoCorte.Visible = false;
                dtpFecha.Value = DateTime.Now;
            }
        }
        #endregion

        #endregion

        #region eventos texbox

        #region codigo KeyPress -> busca codigo en la bd y carga el producto
        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region validar enter
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //limpia txtPago 
                limpiarTxtPago();

                //se crea la variable local codigo
                string codigo = txtCodigo.Text;

                #region validar que el producto exista
                //se valida la existencia del prodcto
                if (producto.cargarProducto(codigo))
                {
                    //se inserta el producto en la tabla dgvProducto
                    dgv.cargarDgvProductos(producto);

                    //se almacena en una tabla los datos del dataGridView
                    cargarTabla();

                    //se hace la suma del dgvProducto y se carga a la etiqueta lTotal
                    sumarTotal();

                    //inicializa la etiqueta lNotificaciones 
                    limpiarNotificaciones();

                    //se inicializa la caja de texto codigo
                    txtCodigo.Text = "";
                }
                else
                {
                    //se manda la notificacion "producto no encontrado" en lNotificaciones
                    crearNotificacion("Producto no encotrado", Color.Red);
                }
                #endregion
            }
            #endregion
        }
        #endregion

        #region codigo TextChanged -> realiza la busqueda de un producto en la misma tabla
        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            if (txtCodigo.Text != "")
            {
                if (botonActivado(cmdBuscarProducto))
                {
                    dgvProductos.Columns["cCantidad"].Visible = false;
                    dgvProductos.Columns["cDescripcion"].Visible = false;
                    dgvProductos.Columns["cPrecioUnitario"].Visible = false;
                    dgvProductos.Columns["cTotal"].Visible = false;
                    cargarTabla();
                    producto.buscarProducto(dgvProductos, txtCodigo.Text, "PRODUCTO");
                    dgv.inicializarDgv();

                    Font fuente = new Font(FontFamily.GenericSansSerif, 14);

                    dgvProductos.RowsDefaultCellStyle.Font = fuente;
                }
            }
            else
            {
                /*dgvProductos.DataSource = null;

                dgvProductos.Columns["cCantidad"].Visible = true;
                dgvProductos.Columns["cDescripcion"].Visible = true;
                dgvProductos.Columns["cPrecioUnitario"].Visible = true;
                dgvProductos.Columns["cTotal"].Visible = true;

                dgvProductos.DataSource = tableIdProductos;*/
            }
        }
        #endregion

        #region pago KeyPress-> valida que sean numeros y calcula el cambio
        private void txtPago_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region valida que sean solo numeros
            //valida que el texto sean solo numeros
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                crearNotificacion("Digite solo numeros", Color.Red);
                e.Handled = true;
            }
            else
            {
                limpiarNotificaciones();
            }
            #endregion

            #region calcular cambio del pago al dar enter
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                limpiarNotificaciones();
                decimal pago = 0;
                decimal total = Convert.ToDecimal(lTotal.Text);
                decimal.TryParse(txtPago.Text, out pago);
                decimal cambio = 0;

                if(pago < total)
                {
                    crearNotificacion("El pago no puede ser menor al total", Color.Red);
                    txtPago.Text = "";
                }
                else
                {
                    cambio = pago - total;
                    lCambio.Text = string.Format("{0:N2}", cambio);
                    e.Handled = false;
                }
            }
            else if (e.KeyChar == Convert.ToChar(Keys.Back))
            {
                e.Handled = false;
            }
            #endregion
        }
        #endregion

        #endregion

        #region eventos dataGridView

        #region romover una fila -> calcula la suma del dgvProducto
        private void dgvProductos_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            limpiarTxtPago();
            sumarTotal();
            cargarTabla();
        }
        #endregion

        #region modificar fila -> calcula la suma del dgvProducto
        private void dgvProductos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvProductos.Rows.Count > 1)
            {
                try
                {
                    limpiarTxtPago();
                    dgv.sumarFilas(e);
                    sumarTotal();
                    cargarTabla();
                }
                catch (Exception)
                {
                    crearNotificacion("Digite solo numeros", Color.Red);
                    dgvProductos.CurrentRow.Cells["cCantidad"].Value = "1";
                }
            }
        }
        #endregion

        #endregion

        #region metodos del formulario

        #region inicializarVariables() -> instancia todos los objetos usados en el formulario
        public void inicializarVariables()
        {
            tableIdProductos = new DataTable();
            producto = new Producto();
            dgv = new GestionDGV(dgvProductos);
            venta = new Venta();
            detalleVenta = new DetalleVenta(tableIdProductos);
            //ticketPrint = new TicketPrint();
            caja = new Caja();
        }
        #endregion


        #region limpiarNotificacion() -> limpia el label lNotificaciones
        public void limpiarNotificaciones()
        {
            crearNotificacion("Notificaciones:", Color.Red);
        }
        #endregion

        #region limpiarTxtpago() -> limpiar textBox pago 
        public void limpiarTxtPago()
        {
            if (txtPago.Text != "")
            {
                txtPago.Text = "0.0";
                lCambio.Text = "0.0";
            }
        }
        #endregion

        #region limpiarRegistro() -> limpia todos los registros despues de la venta;
        public void limpiarRegistro()
        {
            dgvProductos.Rows.Clear();
            limpiarTxtPago();
            //lTotal.Text = "0.0";
            //lCambio.Text = "0.0";
        }
        #endregion

        #region crearNotificacion() -> crea los mesajes de notificacion
        public void crearNotificacion(string mensaje, Color color)
        {
            lNotificaciones.ForeColor = color;
            lNotificaciones.Text = mensaje;
        }
        #endregion


        #region crearColumna() -> crea las columnas para el dataTable tableIdProducto
        public void crearColumna()
        {
            tableIdProductos.Columns.Add("Codigo");
            tableIdProductos.Columns.Add("Cantidad");
            tableIdProductos.Columns.Add("Descripcion");
            tableIdProductos.Columns.Add("Precio U.");
            tableIdProductos.Columns.Add("Total");
        }
        #endregion

        #region cargarTabla() -> carga los datos del dgvProducto al tableIdProducto
        public void cargarTabla()
        {
            tableIdProductos.Rows.Clear();

            DataRow filaNueva;

            foreach (DataGridViewRow filaDatos in dgvProductos.Rows)
            {
                filaNueva = tableIdProductos.NewRow();

                filaNueva["Codigo"] = filaDatos.Cells[0].Value;
                filaNueva["Cantidad"] = filaDatos.Cells[1].Value;
                filaNueva["Descripcion"] = filaDatos.Cells[2].Value;
                filaNueva["Precio U."] = filaDatos.Cells[3].Value;
                filaNueva["Total"] = filaDatos.Cells[4].Value;
          
                tableIdProductos.Rows.Add(filaNueva);
            }
        }
        #endregion

        #region sumarTotal() -> sumar el total del dgvProducto
        public void sumarTotal()
        {
            lTotal.Text = dgv.sumarTotalPagado(dgvProductos);
        }
        #endregion

        #region botonActivado() -> Devuelve si el boton esta presionado
        public bool botonActivado(Button boton)
        {
            if (boton.BackColor == Color.Lime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion

        private void dtpFecha_ValueChanged(object sender, EventArgs e)
        {
            string fecha = dtpFecha.Value.ToString("dd/MM/yyyy");

            lMontoCorte.Text = "$ "+ caja.corte(fecha);
        }

        private void btnActividades_Click(object sender, EventArgs e)
        {
            formAct.Show();
        }

        private void txtPago_TextChanged(object sender, EventArgs e)
        {

        }

        private Boolean validar_cambio(decimal total, decimal pago)
        {
            if(pago >= total)
            {
                return true;
            }
            
            else
            {
                crearNotificacion("El pago es menor al total requerido", Color.Red);
                return false;
            }
        }
    }
}
