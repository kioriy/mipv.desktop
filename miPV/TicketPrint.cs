﻿using System;
using LibPrintTicket;
using System.Data;
using System.Drawing.Printing;

namespace miPV
{
    class TicketPrint
    {
        Ticket ticket;

        public TicketPrint()
        {
            ticket = new Ticket();
        }

        public void imprimirTicket(DataTable tablaIdProductos, string total)
        {
            ticket.FontSize = 6;
            /*mi market j&a
            ramon corona 74 sur
            Centro
            galj670426sk1*/

            /*ticket.AddHeaderLine("STUDIO-8-OCHO");
            ticket.AddHeaderLine("EXPEDIDO EN:");
            ticket.AddHeaderLine("CALZADA FEDERALISTAS 1812-A");
            ticket.AddHeaderLine("COLONIA JARDINES DEL VALLE");
            ticket.AddHeaderLine("TELEFONO: 13714937");*/

            ticket.AddHeaderLine("MI MARKET J & A");
            ticket.AddHeaderLine("EXPEDIDO EN:");
            ticket.AddHeaderLine("RAMON CORONA 74 SUR");
            ticket.AddHeaderLine("COLONIA CENTRO");
            ticket.AddHeaderLine("R.F.C. GALJ670426SK1");

            //ticket.AddSubHeaderLine("Ticket # 1");
            ticket.AddSubHeaderLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());

            foreach(DataRow row in tablaIdProductos.Rows)
            {
                string cantidad = row["Cantidad"].ToString();
                string descripcion = row["Descripcion"].ToString();
                string totalProducto = row["Total"].ToString();

                ticket.AddItem(cantidad, descripcion, totalProducto);
            }
            
            ticket.AddTotal("Total", total);
            //ticket.AddTotal("IVA", "0");
            //ticket.AddTotal("TOTAL", total);
            //ticket.AddTotal("", ""); 
            //ticket.AddTotal("RECIBIDO", "0");
            //ticket.AddTotal("CAMBIO", "0");
            //ticket.AddTotal("", "");

            ticket.AddFooterLine("VUELVA PRONTO");
            
            //ticket.PrintTicket("Microsoft Print to PDF"); //Nombre de la impresora de tickets
            //ticket.PrintTicket("GP-5890X Series");
            ticket.PrintTicket("Microsoft Print to PDF");
        }
    }
}
