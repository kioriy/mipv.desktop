﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miPV
{
    class Query :BD
    {
        #region metodos venta
        public string insertarVenta()
        {
            return "INSERT INTO VENTA(fecha,hora,fk_id_cliente)"+
                   "VALUES((select strftime('%d/%m/%Y', datetime('now'))),(select strftime('%H:%M:%S', datetime('now'))), 1)";
        }

        public string eliminarVenta()
        {
            return "DELETE FROM VENTA WHERE id_venta = (SELECT MAX(id_venta) FROM `VENTA`)";
        }
        #endregion

        #region detalle Venta
        public string insertarDetalleVenta(string cantidad,string idProducto)
        {
            return "INSERT INTO `DETALLE_VENTA`(`cantidad`,`fk_id_venta`,`fk_id_producto`)"+
                   "VALUES ('"+cantidad+ "',(SELECT MAX(id_venta) FROM `VENTA`),'" + idProducto+"')";
        }

        public string eliminarDetalleVenta()
        {
            return "DELETE FROM DETALLE_VENTA WHERE fk_id_venta = (SELECT MAX(id_venta) FROM `VENTA`)";
        }
        #endregion

        #region metodos producto
        public string insertarProducto(string codigo, string precioVenta, string precioCosto, string descripcion, string cantidad)
        {
            return "INSERT INTO PRODUCTO(codigo, precio_venta, precio_costo, descripcion, cantidad, fk_id_proveedor)" +
                   "VALUES('"+codigo+"','"+precioVenta+"','"+precioCosto+"','"+descripcion+"','"+cantidad+"', 0)";  
        }

        public string cargarProducto(string codigo)
        {
            return "SELECT id_producto, codigo, precio_venta, precio_costo, descripcion, cantidad, fk_id_proveedor FROM PRODUCTO WHERE  codigo = '" + codigo + "'";
        }

        public string buscarProducto(string descripcion)
        {
            return "SELECT codigo AS CODIGO, descripcion AS DESCRIPCION, precio_venta AS 'PRECIO VENTA', precio_costo AS 'PRECIO COSTO' FROM PRODUCTO WHERE descripcion LIKE '%" + descripcion+"%'";
        }

        public string actualizarProducto(string codigo, string precioVenta, string precioCosto, string descripcion, string cantidad, string idProducto)
        {
            return "UPDATE `PRODUCTO` SET "+
                   "`codigo`= '"+codigo+"', `descripcion`= '"+descripcion+"', precio_venta = '"+precioVenta+"', "+
                   "precio_costo = '"+precioCosto+"', cantidad = '"+cantidad+"'"+
                   "WHERE id_producto = '"+idProducto+"'";
        }
        #endregion

        #region caja
        public string corteCaja(string fecha)
        {
            return "SELECT SUM(p.precio_venta) FROM PRODUCTO AS p INNER JOIN DETALLE_VENTA AS d ON d.fk_id_producto = p.id_producto INNER JOIN VENTA AS v ON  v.id_venta =  d.fk_id_venta WHERE v.fecha = '"+fecha+"'";
        }
        #endregion

        #region usuario
        public string insertarUsuario(string nombre, string contraseña, string roll)
        {
            return $"INSERT INTO USUARIO(`nombre`,`contraseña`,`roll`) VALUES (`{nombre}`,`{contraseña}`,`{roll}`";
        }

        public string buscarUsuario(string nombre)
        {
            return $"SELECT `{nombre}` FROM USUARIO WHERE `nombre` = {nombre}`";
        }

        public string actualizarUsuario(string nombre, string contraseña, string roll, string id)
        {
            return $"UPDATE USUARIO SET nombre = '{nombre}', contraseña = '{contraseña}', roll = `{roll}` WHERE id_usuario = '{id}'";
        }

        #endregion

        public string queryConsulta(string atributo, string tabla, string condicion)
        {
            return $"SELECT {atributo} FROM {tabla} {condicion}";
        }

        public string llenarComboBox(string columna, string tabla)
        {
            return "SELECT "+columna+" FROM "+tabla+" ORDER BY "+columna+" ASC"; 
        }
    }
}
