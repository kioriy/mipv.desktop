﻿using System.Data;
using System.Data.SQLite;
using System.Configuration;

public class BD
{
    #region variables
    string parametrosConexion = ConfigurationManager.ConnectionStrings["db"].ConnectionString;
    SQLiteConnection conexionBD;
    SQLiteCommand queryCommand;
    public SQLiteDataAdapter adaptador;
    public DataSet obtenerDatos;
    #endregion

    #region Abrir y cerrar conexion
    public bool conexionAbrir()
    {
        conexionBD = new SQLiteConnection(parametrosConexion);
        ConnectionState estadoConexion;
        try
        {
            conexionBD.Open();
            estadoConexion = conexionBD.State;
            return true;
        }
        catch (SQLiteException)
        {
            return false;
        }

    }
    public void conexionCerrar()
    {
        if (conexionBD.State == ConnectionState.Open)
        {
            conexionBD.Close();
        }
    }
    #endregion


    #region metodos


    public bool insertar(string sqliteInsertar)
    {
        int filasafectadas = 0;
        conexionAbrir();

        queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
        filasafectadas = queryCommand.ExecuteNonQuery();

        conexionCerrar();

        if (filasafectadas > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region consulta
    public DataSet consulta(string queryConsulta, string tabla)
    {
        conexionAbrir();

        obtenerDatos = new DataSet();
        obtenerDatos.Reset();

        adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD);

        adaptador.Fill(obtenerDatos, tabla);

        conexionCerrar();

        return obtenerDatos;
    }
    #endregion

    #region abrir conexion a la base de datos
    public bool abrirConexion()
    {
        conexionBD = new SQLiteConnection(parametrosConexion);
        ConnectionState estadoConexion;
  
        try
        {
            conexionBD.Open(); estadoConexion = conexionBD.State;
            return true;
        }
        catch (SQLiteException)
        {
            return false;
        }
    }
    #endregion

    #region cerrar conexion a la base de datos
    private void cerrarConexion()
    {
        if (conexionBD.State == ConnectionState.Open)
        {
            conexionBD.Close();
        }
    }
    #endregion

    #region ejecuta la instruccion sql insertar y update
    public bool ejecutaComando(string query)
    {
        int filasAfectadas = 0;
        abrirConexion();

        queryCommand = new SQLiteCommand(query, conexionBD);
        filasAfectadas = queryCommand.ExecuteNonQuery();
        cerrarConexion();

        if (filasAfectadas > 0)
        {
            return true;
        }
        else {
            return false;
        }
    }
    #endregion

    #region Adaptador -> carga los datos de una consulta
    public SQLiteDataAdapter consultaAdaptador(string query)
    {
        abrirConexion();
        adaptador = new SQLiteDataAdapter(query, conexionBD);
        cerrarConexion();
        return adaptador;
    }
    #endregion

    #region llena un comboBox con una consulta sql
    public void llenarCombobox(System.Windows.Forms.ComboBox llenarComBox, string query, string columna, string tabla, ref int entra)
    {
        obtenerDatos = new DataSet();
        obtenerDatos.Reset();
        adaptador = consultaAdaptador(query);
        adaptador.Fill(obtenerDatos, tabla);
        entra = 1;
        llenarComBox.DataSource = obtenerDatos.Tables[tabla];
        llenarComBox.DisplayMember = columna;
        llenarComBox.SelectedIndex = -1;
        entra = 0;
    }
    #endregion

    #region llena un listBox con una consulta sql
    public void llenarListBox(System.Windows.Forms.ListBox llenarListBox, string query, string columna, string tabla, ref int entra)
    {
        obtenerDatos = new DataSet();
        obtenerDatos.Reset();
        adaptador = consultaAdaptador(query);
        adaptador.Fill(obtenerDatos, tabla);
        entra = 1;
        llenarListBox.DataSource = obtenerDatos.Tables[tabla];
        llenarListBox.DisplayMember = columna;
        llenarListBox.SelectedIndex = -1;
        entra = 0;
    }
    #endregion

    #region llena un dataGridView con una consulta sql
    public void llenarDgv(System.Windows.Forms.DataGridView dgvLlenar, string query, string tabla)
    {
        obtenerDatos = new DataSet();
        obtenerDatos.Reset();

        adaptador = consultaAdaptador(query);
        adaptador.Fill(obtenerDatos, tabla);

        dgvLlenar.DataSource = obtenerDatos.Tables[0];
    }
    #endregion

    #endregion
}