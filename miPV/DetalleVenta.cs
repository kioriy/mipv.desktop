﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace miPV
{
    class DetalleVenta
    {
        #region variables
        string idDetalleVenta = "";
        string cantidad = "";
        string idVenta = "";
        string idProducto = "";
 
        DataTable tableIdProducto = new DataTable();
        BD bd;
        Query query;
        #endregion

        #region constructores
        public DetalleVenta(DataTable tableIdProducto)
        {
            this.tableIdProducto = tableIdProducto;
            bd = new BD();
            query = new Query();
        }
        #endregion

        #region metodos

        #region insertar detalle venta
        public bool insertarDetalleVenta()
        {
            foreach (DataRow fila in tableIdProducto.Rows)
            {
                idProducto = fila["Codigo"].ToString();
                cantidad = fila["Cantidad"].ToString();

                if(!bd.ejecutaComando(query.insertarDetalleVenta(this.cantidad, this.idProducto)))
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region eliminar detalle de venta
        public bool eliminarDetalleVenta()
        {
            return bd.ejecutaComando(query.eliminarDetalleVenta());
        }
        #endregion

        #endregion
    }
}
