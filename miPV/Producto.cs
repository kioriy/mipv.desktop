﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace miPV
{
    class Producto
    {
        #region atributos
        public List<Producto> listaIdProducto;
        private string idProducto = "";
        private string codigo = "";
        private string precioCosto = "";
        private string precioVenta = "";
        private string descripcion = "";
        private string cantidad = "";
        //private string respuesta = "";
        private BD bd;
        private Query query;
        private DataSet obtenerDatos;
        #endregion

        #region constructores
        /// <summary>
        /// Recibe 4 parametros
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="descripcion"></param>
        /// <param name="cantidad"></param>
        /// <param name="precioCosto"></param>
        /// <param name="precioVenta"></param>
        public Producto(string codigo, string descripcion, string cantidad, string precioCosto, string precioVenta)
        {
            this.bd = new BD();
            this.query = new Query();

            this.codigo = codigo;
            this.precioCosto = precioCosto;
            this.precioVenta = precioVenta;
            this.descripcion = descripcion;
            this.cantidad = cantidad;
        }

        /// <summary>
        /// Recibe como parametro el codigo del producto;
        /// </summary>
        /// <param name="codigo"></param>
        public Producto(string codigo)
        {
            this.bd = new BD();
            this.query = new Query();
            this.codigo = codigo;
        }

        public Producto()
        {
            this.bd = new BD();
            this.query = new Query();
            listaIdProducto = new List<Producto>();
        }
        #endregion

        #region metodos

        #region insertarProducto();
        public bool insertarProducto()
        { 
            return bd.ejecutaComando(query.insertarProducto(this.codigo, this.precioCosto, this.precioVenta, this.descripcion, this.cantidad));
        }
        #endregion

        #region insertarProducto() -> Sobrecarga
        public bool insertarProducto(string codigo, string descripcion, string cantidad, string precioCosto, string precioVenta)
        {
            return bd.ejecutaComando(query.insertarProducto(codigo, precioCosto, precioVenta, descripcion, cantidad));
        }

        public bool actualizarProducto(string codigo, string descripcion, string cantidad, string precioCosto, string precioVenta, string idProducto)
        {
            return bd.ejecutaComando(query.actualizarProducto(codigo, precioCosto, precioVenta, descripcion, cantidad, idProducto));
        }
        #endregion

        #region cargar producto
        /// <summary>
        /// Para cargar el producto toma como parametro el codgio en el constructor
        /// </summary>
        /// <returns></returns>
        public bool cargarProducto()
        {
            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(query.cargarProducto(this.codigo));
            bd.adaptador.Fill(obtenerDatos, "PRODUCTO");

            try
            {
                this.idProducto = obtenerDatos.Tables["PRODUCTO"].Rows[0][0].ToString();
                this.codigo = obtenerDatos.Tables["PRODUCTO"].Rows[0][1].ToString(); 
                this.precioCosto = obtenerDatos.Tables["PRODUCTO"].Rows[0][2].ToString(); 
                this.precioVenta = obtenerDatos.Tables["PRODUCTO"].Rows[0][3].ToString(); 
                this.descripcion = obtenerDatos.Tables["PRODUCTO"].Rows[0][4].ToString(); 
                this.cantidad = obtenerDatos.Tables["PRODUCTO"].Rows[0][5].ToString();

                return true; 
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
        #endregion

        #region cargar producto -> con sobre carga el parametro codigo
        /// <summary>
        /// usa el metodo para cargar el producto enviando la variable codigo
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        public bool cargarProducto(string codigo)
        {
            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            bd.adaptador = bd.consultaAdaptador(query.cargarProducto(codigo));
            bd.adaptador.Fill(obtenerDatos, "PRODUCTO");

            try
            {
                this.idProducto = obtenerDatos.Tables["PRODUCTO"].Rows[0][0].ToString();
                this.codigo = obtenerDatos.Tables["PRODUCTO"].Rows[0][1].ToString();
                this.precioCosto = obtenerDatos.Tables["PRODUCTO"].Rows[0][2].ToString();
                this.precioVenta = obtenerDatos.Tables["PRODUCTO"].Rows[0][3].ToString();
                this.descripcion = obtenerDatos.Tables["PRODUCTO"].Rows[0][4].ToString();
                this.cantidad = obtenerDatos.Tables["PRODUCTO"].Rows[0][5].ToString();

                return true;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
        #endregion

        #region buscarProducto() -> 
        public void buscarProducto(DataGridView dgv, string descripcion, string tabla)
        {
            bd.llenarDgv(dgv, query.buscarProducto(descripcion), tabla);
        }
        #endregion

        #endregion

        #region set get variables
        public string IdProducto
        {
            get
            {
                return idProducto;
            }

            set
            {
                idProducto = value;
            }
        }

        public string Codigo
        {
            get
            {
                return codigo;
            }

            set
            {
                codigo = value;
            }
        }

        public string PrecioCosto
        {
            get
            {
                return precioCosto;
            }

            set
            {
                precioCosto = value;
            }
        }

        public string PrecioVenta
        {
            get
            {
                return precioVenta;
            }

            set
            {
                precioVenta = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }

        public string Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }
        #endregion
    }
}
