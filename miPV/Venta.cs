﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miPV
{
    class Venta
    {
        #region variables
        string idVenta = "";
        string fecha = "";
        string idCliente = "";

        BD bd;
        Query query;
        Actividades actividad = new Actividades();
        #endregion

        #region constructores
        public Venta()
        {
            bd = new BD();
            query = new Query();
        }
        #endregion

        #region metodos

        #region inserta venta
        public bool insertarVenta()
        {
            //Check de insertar venta
            actividad.insertar_actividad("Registro venta", "Persona X");
            return bd.ejecutaComando(query.insertarVenta());
        }
        #endregion

        #region cargarVenta
        public void cargarVenta()
        {

        }
        #endregion

        #region eliminar venta
        public bool eliminarVenta()
        {
            //Check de Eliminar venta
            actividad.insertar_actividad("Elimino venta", "Persona X");
            return bd.ejecutaComando(query.eliminarVenta());
        }
        #endregion

        #endregion
    }
}
