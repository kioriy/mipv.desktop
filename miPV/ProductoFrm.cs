﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miPV
{
    public partial class ProductoFrm : Form
    {
        #region variables
        BD bd;
        Producto producto;
        Query query;
        Actividades actividad = new Actividades();

        string idProducto;
        string codigo;
        string descripcion;
        string cantidad;
        string precioCosto;
        string precioVenta;
        int entra = 0;
        #endregion

        #region constructor
        public ProductoFrm()
        {
            InitializeComponent();
            inicializarVariables();
            bd.llenarCombobox(cbCodigo, query.llenarComboBox("codigo", "PRODUCTO"),"codigo", "PRODUCTO", ref entra);
        }
        #endregion

        #region botones

        #region agregar Producto
        private void cmdAgregarProducto_Click(object sender, EventArgs e)
        {
            if (validarDatos())
            {
                cargarVariables();

                if (producto.insertarProducto(codigo, descripcion, cantidad, precioCosto, precioVenta))
                {
                    crearNotificacion("Producto registrado", Color.Green);
                    limpiarRegistro();
                    bd.llenarCombobox(cbCodigo, query.llenarComboBox("codigo", "PRODUCTO"), "codigo", "PRODUCTO", ref entra);
                    //check de registro de producto
                    actividad.insertar_actividad("Registro producto", "Persona X");
                }
            }
            else
            {
                crearNotificacion("Llenar todos los datos", Color.Red);
            }
        }

        private void cmdActualizar_Click(object sender, EventArgs e)
        {
            if (compararDatos())
            {
                cargarVariables();
                producto.actualizarProducto(codigo, descripcion, cantidad, precioCosto, precioVenta, idProducto);
                crearNotificacion("Producto actualizado", Color.Green);
                limpiarRegistro();
                cmdAgregarProducto.Visible = true;
                cmdActualizar.Visible = false;
                bd.llenarCombobox(cbCodigo, query.llenarComboBox("codigo", "PRODUCTO"),"codigo", "PRODUCTO", ref entra);
            }
            else
            {
                crearNotificacion("No hay cambios por aplicar", Color.Orange);
            }
        }
        #endregion

        #endregion

        #region eventos comboBox
        private void cbCodigo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entra == 0)
            {
                if (cbCodigo.Text != "" && cbCodigo.Text != "1") 
                {
                    crearNotificacion("Notificaciones:", Color.Red);
                    cargarVariablesDeClase();
                    cmdAgregarProducto.Visible = false;
                    cmdActualizar.Visible = true;
                }
            }
        }
        #endregion

        #region metodos formulario

        #region cargarVariables() ->
        public void cargarVariables()
        {
            if (validarDatos())
            {
                codigo = cbCodigo.Text;
                descripcion = txtDescripcion.Text;
                cantidad = txtCantidad.Text;
                precioCosto = txtPrecioCosto.Text;
                precioVenta = txtPrecioVenta.Text;
            }
        }

        public void cargarVariablesDeClase()
        {
            producto.cargarProducto(cbCodigo.Text);

            idProducto = producto.IdProducto;
            txtDescripcion.Text = producto.Descripcion;
            txtCantidad.Text = producto.Cantidad;
            txtPrecioCosto.Text = producto.PrecioCosto;
            txtPrecioVenta.Text = producto.PrecioVenta;
        }

        public bool compararDatos()
        {
            if (cbCodigo.Text == producto.Codigo && txtDescripcion.Text == producto.Descripcion && txtCantidad.Text == producto.Cantidad && txtPrecioCosto.Text == producto.PrecioCosto && txtPrecioVenta.Text == producto.PrecioVenta)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region validarDatos() ->
        public bool validarDatos()
        {
            if (cbCodigo.Text == "")
            {
                return false;
            }
            else if (txtDescripcion.Text == "")
            {
                return false;
            }
           /* else if (txtCantidad.Text == "")
            {
                return false;
            }*/
            else if (txtPrecioCosto.Text == "")
            {
                return false;
            }
            else if (txtPrecioVenta.Text == "")
            {
                return false;
            }
            
            return true;
        }
        #endregion

        #region inicializarVariables() -> instancia todos los objetos usados en el formulario
        public void inicializarVariables()
        {
            //tableIdProductos = new DataTable();
            producto = new Producto();
            bd = new BD();
            query = new Query();
            //dgv = new GestionDGV(dgvProductos);
            //venta = new Venta();
            //detalleVenta = new DetalleVenta(tableIdProductos);
            //ticketPrint = new TicketPrint();

        }
        #endregion

        #region crearNotificacion() -> crea los mesajes de notificacion
        public void crearNotificacion(string mensaje, Color color)
        {
            lNotificaciones.ForeColor = color;
            lNotificaciones.Text = mensaje;
        }
        #endregion

        #region limpiarNotificacion() -> limpia el label lNotificaciones
        public void limpiarNotificaciones()
        {
            crearNotificacion("Notificaciones:", Color.Red);
        }
        #endregion

        #region limpiarRegistro() -> limpia todos los registros despues de la venta;
        public void limpiarRegistro()
        {
            cbCodigo.Text = "";
            txtDescripcion.Text = "";
            //txtCantidad.Text = "";
            txtPrecioCosto.Text = "";
            txtPrecioVenta.Text = "";
        }
        #endregion

        #endregion
    }
}
