﻿namespace miPV
{
    partial class PuntoDeVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PuntoDeVenta));
            this.dgvProductos = new System.Windows.Forms.DataGridView();
            this.cmdPagar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPago = new System.Windows.Forms.TextBox();
            this.cmdProductos = new System.Windows.Forms.Button();
            this.lProducto = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lTotal = new System.Windows.Forms.Label();
            this.lCambio = new System.Windows.Forms.Label();
            this.lNotificaciones = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmdBuscarProducto = new System.Windows.Forms.Button();
            this.cmdCorteCaja = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lMontoCorte = new System.Windows.Forms.Label();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.Actividades = new System.Windows.Forms.Button();
            this.btnActividades = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvProductos
            // 
            this.dgvProductos.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvProductos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProductos.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvProductos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvProductos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProductos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProductos.Location = new System.Drawing.Point(12, 49);
            this.dgvProductos.Name = "dgvProductos";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProductos.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProductos.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProductos.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvProductos.RowTemplate.Height = 35;
            this.dgvProductos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductos.Size = new System.Drawing.Size(796, 293);
            this.dgvProductos.TabIndex = 0;
            this.dgvProductos.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductos_CellValueChanged);
            this.dgvProductos.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvProductos_RowsRemoved);
            // 
            // cmdPagar
            // 
            this.cmdPagar.BackColor = System.Drawing.Color.Transparent;
            this.cmdPagar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdPagar.BackgroundImage")));
            this.cmdPagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdPagar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cmdPagar.FlatAppearance.BorderSize = 0;
            this.cmdPagar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.cmdPagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmdPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPagar.Location = new System.Drawing.Point(532, 348);
            this.cmdPagar.Name = "cmdPagar";
            this.cmdPagar.Size = new System.Drawing.Size(117, 75);
            this.cmdPagar.TabIndex = 3;
            this.cmdPagar.UseVisualStyleBackColor = false;
            this.cmdPagar.Click += new System.EventHandler(this.cmdPagar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 345);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Pago";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(413, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "Cambio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(283, 345);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Total";
            // 
            // txtPago
            // 
            this.txtPago.BackColor = System.Drawing.SystemColors.MenuText;
            this.txtPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPago.ForeColor = System.Drawing.Color.Lime;
            this.txtPago.Location = new System.Drawing.Point(15, 373);
            this.txtPago.Name = "txtPago";
            this.txtPago.Size = new System.Drawing.Size(176, 53);
            this.txtPago.TabIndex = 2;
            this.txtPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPago.TextChanged += new System.EventHandler(this.txtPago_TextChanged);
            this.txtPago.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPago_KeyPress);
            // 
            // cmdProductos
            // 
            this.cmdProductos.BackColor = System.Drawing.Color.Transparent;
            this.cmdProductos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdProductos.BackgroundImage")));
            this.cmdProductos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdProductos.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cmdProductos.FlatAppearance.BorderSize = 0;
            this.cmdProductos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.cmdProductos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmdProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdProductos.Location = new System.Drawing.Point(814, 49);
            this.cmdProductos.Name = "cmdProductos";
            this.cmdProductos.Size = new System.Drawing.Size(117, 75);
            this.cmdProductos.TabIndex = 11;
            this.cmdProductos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdProductos.UseVisualStyleBackColor = false;
            this.cmdProductos.Click += new System.EventHandler(this.cmdProductos_Click);
            // 
            // lProducto
            // 
            this.lProducto.AutoSize = true;
            this.lProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lProducto.Location = new System.Drawing.Point(829, 127);
            this.lProducto.Name = "lProducto";
            this.lProducto.Size = new System.Drawing.Size(86, 24);
            this.lProducto.TabIndex = 12;
            this.lProducto.Text = "Producto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 24);
            this.label5.TabIndex = 13;
            this.label5.Text = "codigo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BackColor = System.Drawing.SystemColors.Control;
            this.txtCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.ForeColor = System.Drawing.Color.Red;
            this.txtCodigo.Location = new System.Drawing.Point(79, 13);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(684, 31);
            this.txtCodigo.TabIndex = 1;
            this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // lTotal
            // 
            this.lTotal.AutoSize = true;
            this.lTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lTotal.Location = new System.Drawing.Point(253, 376);
            this.lTotal.Name = "lTotal";
            this.lTotal.Size = new System.Drawing.Size(75, 46);
            this.lTotal.TabIndex = 16;
            this.lTotal.Text = "0.0";
            // 
            // lCambio
            // 
            this.lCambio.AutoSize = true;
            this.lCambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCambio.ForeColor = System.Drawing.Color.Blue;
            this.lCambio.Location = new System.Drawing.Point(420, 376);
            this.lCambio.Name = "lCambio";
            this.lCambio.Size = new System.Drawing.Size(75, 46);
            this.lCambio.TabIndex = 17;
            this.lCambio.Text = "0.0";
            // 
            // lNotificaciones
            // 
            this.lNotificaciones.AutoSize = true;
            this.lNotificaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lNotificaciones.ForeColor = System.Drawing.Color.Red;
            this.lNotificaciones.Location = new System.Drawing.Point(12, 439);
            this.lNotificaciones.Name = "lNotificaciones";
            this.lNotificaciones.Size = new System.Drawing.Size(153, 25);
            this.lNotificaciones.TabIndex = 18;
            this.lNotificaciones.Text = "Notificaciones:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(215, 376);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 46);
            this.label6.TabIndex = 19;
            this.label6.Text = "$";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(382, 376);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 46);
            this.label7.TabIndex = 20;
            this.label7.Text = "$";
            // 
            // cmdBuscarProducto
            // 
            this.cmdBuscarProducto.BackColor = System.Drawing.Color.Transparent;
            this.cmdBuscarProducto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdBuscarProducto.BackgroundImage")));
            this.cmdBuscarProducto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdBuscarProducto.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cmdBuscarProducto.FlatAppearance.BorderSize = 0;
            this.cmdBuscarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdBuscarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Lime;
            this.cmdBuscarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdBuscarProducto.Location = new System.Drawing.Point(769, 12);
            this.cmdBuscarProducto.Name = "cmdBuscarProducto";
            this.cmdBuscarProducto.Size = new System.Drawing.Size(39, 32);
            this.cmdBuscarProducto.TabIndex = 21;
            this.cmdBuscarProducto.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdBuscarProducto.UseVisualStyleBackColor = false;
            this.cmdBuscarProducto.Click += new System.EventHandler(this.cmdBuscarProducto_Click);
            // 
            // cmdCorteCaja
            // 
            this.cmdCorteCaja.BackColor = System.Drawing.Color.Transparent;
            this.cmdCorteCaja.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cmdCorteCaja.BackgroundImage")));
            this.cmdCorteCaja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cmdCorteCaja.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cmdCorteCaja.FlatAppearance.BorderSize = 0;
            this.cmdCorteCaja.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.cmdCorteCaja.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmdCorteCaja.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCorteCaja.Location = new System.Drawing.Point(814, 203);
            this.cmdCorteCaja.Name = "cmdCorteCaja";
            this.cmdCorteCaja.Size = new System.Drawing.Size(117, 75);
            this.cmdCorteCaja.TabIndex = 22;
            this.cmdCorteCaja.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdCorteCaja.UseVisualStyleBackColor = false;
            this.cmdCorteCaja.Click += new System.EventHandler(this.cmdCorteCaja_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(814, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 24);
            this.label4.TabIndex = 23;
            this.label4.Text = "Corte de caja";
            // 
            // lMontoCorte
            // 
            this.lMontoCorte.AutoSize = true;
            this.lMontoCorte.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMontoCorte.Location = new System.Drawing.Point(843, 318);
            this.lMontoCorte.Name = "lMontoCorte";
            this.lMontoCorte.Size = new System.Drawing.Size(25, 24);
            this.lMontoCorte.TabIndex = 25;
            this.lMontoCorte.Text = "...";
            this.lMontoCorte.Visible = false;
            // 
            // dtpFecha
            // 
            this.dtpFecha.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(819, 177);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(109, 20);
            this.dtpFecha.TabIndex = 26;
            this.dtpFecha.Visible = false;
            this.dtpFecha.ValueChanged += new System.EventHandler(this.dtpFecha_ValueChanged);
            // 
            // Actividades
            // 
            this.Actividades.Location = new System.Drawing.Point(0, 0);
            this.Actividades.Name = "Actividades";
            this.Actividades.Size = new System.Drawing.Size(75, 23);
            this.Actividades.TabIndex = 28;
            // 
            // btnActividades
            // 
            this.btnActividades.BackColor = System.Drawing.Color.Transparent;
            this.btnActividades.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnActividades.BackgroundImage")));
            this.btnActividades.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnActividades.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnActividades.FlatAppearance.BorderSize = 0;
            this.btnActividades.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnActividades.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnActividades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActividades.Location = new System.Drawing.Point(681, 351);
            this.btnActividades.Name = "btnActividades";
            this.btnActividades.Size = new System.Drawing.Size(117, 75);
            this.btnActividades.TabIndex = 27;
            this.btnActividades.UseVisualStyleBackColor = false;
            this.btnActividades.Click += new System.EventHandler(this.btnActividades_Click);
            // 
            // PuntoDeVenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(940, 477);
            this.Controls.Add(this.btnActividades);
            this.Controls.Add(this.Actividades);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.lMontoCorte);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmdCorteCaja);
            this.Controls.Add(this.cmdBuscarProducto);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lNotificaciones);
            this.Controls.Add(this.lCambio);
            this.Controls.Add(this.lTotal);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lProducto);
            this.Controls.Add(this.cmdProductos);
            this.Controls.Add(this.txtPago);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdPagar);
            this.Controls.Add(this.dgvProductos);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PuntoDeVenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Punto de venta - ac 10";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProductos;
        private System.Windows.Forms.Button cmdPagar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPago;
        private System.Windows.Forms.Button cmdProductos;
        private System.Windows.Forms.Label lProducto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lTotal;
        private System.Windows.Forms.Label lCambio;
        private System.Windows.Forms.Label lNotificaciones;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdBuscarProducto;
        private System.Windows.Forms.Button cmdCorteCaja;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lMontoCorte;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Button Actividades;
        private System.Windows.Forms.Button btnActividades;
    }
}

